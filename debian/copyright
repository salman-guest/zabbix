Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Zabbix
Upstream-Contact: Alexei Vladishev <alexei.vladishev@zabbix.com>
Source: http://www.zabbix.com/download.php
Copyright: 2000-2022 Zabbix SIA
License: GPL-2+
Files-Excluded:
    bin/win*/*.exe
    bin/win*/dev/*.dll
    bin/win*/dev/*.lib
    src/zabbix_java/lib/*.jar
    src/go/vendor
Files-Excluded-templates:
    net/cisco/cisco_catalyst*
Files-Excluded-vendor:
    vendor/github.com/BurntSushi/locker
  ~~vendor/github.com/chromedp/sysutil
    vendor/github.com/eclipse/paho.mqtt.golang
    vendor/github.com/fsnotify/fsnotify
    vendor/github.com/goburrow/modbus
    vendor/github.com/goburrow/serial
    vendor/github.com/godbus/dbus
    vendor/github.com/go-ldap/ldap
    vendor/github.com/go-logfmt/logfmt
    vendor/github.com/go-sql-driver/mysql
  ~~vendor/github.com/jackc/chunkreader
  ~~vendor/github.com/jackc/pgconn
  ~~vendor/github.com/jackc/pgio
  ~~vendor/github.com/jackc/pgpassfile
  ~~vendor/github.com/jackc/pgproto3
  ~~vendor/github.com/jackc/pgservicefile
  ~~vendor/github.com/jackc/pgtype
   ~vendor/github.com/jackc/pgx
  ~~vendor/github.com/jackc/puddle
    vendor/github.com/josharian/intern
    vendor/github.com/mailru/easyjson
    vendor/github.com/mattn/go-sqlite3
    vendor/github.com/miekg/dns
  ~~vendor/golang.org/x/sys
  ~~vendor/golang.org/x/net
    vendor/golang.org/x/xerrors
    vendor/gopkg.in
Comment:
 This package was debianized by Michael Ablassmeier <abi@debian.org>
 on Tue, 23 Mar 2004 22:46:45 +0100.
 .
 "~~" denotes bundled dependencies not present in Bullseye-Backports.

Files: *
Copyright: 2000-2022 Zabbix SIA
License: GPL-2+
Comment:
 Author: Alexei Vladishev <alexei.vladishev@zabbix.com>

Files:
    src/libs/zbxembed/duktape.c
Copyright:
    2013-2019 Duktape authors
License: Expat

Files:
    src/libs/zbxcommon/zbxgetopt.c
    include/zbxgetopt.h
Copyright:
    1985,1987,1988-1993 Free Software Foundation, Inc
License: GPL-2+

Files:
    src/libs/zbxcrypto/hmac_sha256.*
Copyright: N/A
License: Public-Domain~Unlicense
 This is free and unencumbered software released into the public domain.
 ․
 Anyone is free to copy, modify, publish, use, compile, sell, or
 distribute this software, either in source code form or as a compiled
 binary, for any purpose, commercial or non-commercial, and by any
 means.
 ․
 In jurisdictions that recognize copyright laws, the author or authors
 of this software dedicate any and all copyright interest in the
 software to the public domain. We make this dedication for the benefit
 of the public at large and to the detriment of our heirs and
 successors. We intend this dedication to be an overt act of
 relinquishment in perpetuity of all present and future rights to this
 software under copyright law.
 ․
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 ․
 For more information, please refer to <http://unlicense.org/>
Comment: https://github.com/h5p9sl/hmac_sha256/blob/master/LICENSE

Files:
    include/md5.h
    src/libs/zbxcrypto/md5.c
Copyright: 1999,2000,2002 Aladdin Enterprises.
License: custom-BSD-like
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.
 .
 L. Peter Deutsch
 ghost@aladdin.com

Files: src/libs/zbxcommon/str.c
Copyright: 2000-2022 Zabbix SIA
License: GPL-2+
Comment: with portions
 Copyright: 1998 Todd C. Miller <Todd.Miller@courtesan.com>
  Permission to use, copy, modify, and distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.
  .
  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

Files: src/libs/zbxcrypto/sha*crypt.c
Copyright: N/A
License: Public-Domain
 Released into the Public Domain by Ulrich Drepper <drepper@redhat.com>

Files: misc/init.d/suse/*
Copyright: 2004 A.Tophofen
License: GPL-2+
Comment: assuming the same license as for the whole application

Files: misc/images/png_classic/*.png
Copyright: Jakub "Jimmac" Steiner
License: GPL-2+

Files: ui/js/vendors/jquery.js
Copyright:
    2005-2019 John Resig, Brandon Aaron & Jörn Zaefferer
              JS Foundation and other contributors
License: Expat
Comment:
 Minified; full source is in "debian/missing-sources".

Files: ui/js/vendors/jquery-ui.js
Copyright:
    2012-2019 jQuery Foundation and other contributors
License: Expat
Comment:
 Minified; full source is in "debian/missing-sources".

Files: ui/js/vendors/Leaflet/Leaflet.markercluster/leaflet.markercluster.js
Copyright: 2012 Literate Programs
License: Expat
Comment: https://literateprograms.org/quickhull__javascript_.html

Files: ui/js/vendors/Leaflet/Leaflet/*
Copyright:
    2010-2021, Vladimir Agafonkin
    2010-2011, CloudMade
License: BSD-2-Clause
 Redistribution and use in source and binary forms, with or without modification, are
 permitted provided that the following conditions are met:
 .
   1. Redistributions of source code must retain the above copyright notice, this list of
      conditions and the following disclaimer.
 .
   2. Redistributions in binary form must reproduce the above copyright notice, this list
      of conditions and the following disclaimer in the documentation and/or other materials
      provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
Comment: https://github.com/Leaflet/Leaflet

Files: ui/vendor/composer/*
Copyright: Nils Adermann, Jordi Boggiano
License: Expat

Files:
    ui/vendor/onelogin/*
Copyright:
    2010-2016 OneLogin, Inc.
License: Expat

Files: ui/vendor/symfony/*
Copyright: 2004-2020 Fabien Potencier
License: Expat

Files:
    ui/vendor/robrichards/xmlseclibs/*
Copyright:
    2007-2019 Robert Richards <rrichards@cdatazone.org>
License: BSD-3-Clause~RR
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
  * Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
  * Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.
 .
  * Neither the name of Robert Richards nor the names of his
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: ui/assets/fonts/*.ttf
Copyright: 2003 Bitstream, Inc. All Rights Reserved.
 Bitstream Vera is a trademark of Bitstream, Inc.
 DejaVu changes are in public domain.
License: bitstream-font-license
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of the fonts accompanying this license ("Fonts") and associated
 documentation files (the "Font Software"), to reproduce and distribute the
 Font Software, including without limitation the rights to use, copy, merge,
 publish, distribute, and/or sell copies of the Font Software, and to permit
 persons to whom the Font Software is furnished to do so, subject to the
 following conditions:
 .
 The above copyright and trademark notices and this permission notice shall
 be included in all copies of one or more of the Font Software typefaces.
 .
 The Font Software may be modified, altered, or added to, and in particular
 the designs of glyphs or characters in the Fonts may be modified and
 additional glyphs or characters may be added to the Fonts, only if the fonts
 are renamed to names not containing either the words "Bitstream" or the word
 "Vera".
 .
 This License becomes null and void to the extent applicable to Fonts or Font
 Software that has been modified and is distributed under the "Bitstream
 Vera" names.
 .
 The Font Software may be sold as part of a larger software package but no
 copy of one or more of the Font Software typefaces may be sold by itself.
 .
 THE FONT SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF COPYRIGHT, PATENT,
 TRADEMARK, OR OTHER RIGHT. IN NO EVENT SHALL BITSTREAM OR THE GNOME
 FOUNDATION BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, INCLUDING
 ANY GENERAL, SPECIAL, INDIRECT, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
 THE USE OR INABILITY TO USE THE FONT SOFTWARE OR FROM OTHER DEALINGS IN THE
 FONT SOFTWARE.
 .
 Except as contained in this notice, the names of Gnome, the Gnome
 Foundation, and Bitstream Inc., shall not be used in advertising or
 otherwise to promote the sale, use or other dealings in this Font Software
 without prior written authorization from the Gnome Foundation or Bitstream
 Inc., respectively. For further information, contact: fonts at gnome dot
 org.

Files:
    templates/media/mattermost/*
Copyright:
    2001-2022 Zabbix SIA
License: GPL-2 or Apache-2.0

Files:
    vendor/github.com/cakturk/go-netstat/*
Copyright:
    2018 Cihangir Akturk
License: Expat

Files:
    vendor/github.com/chromedp/*
Copyright:
    2016-2020 Kenneth Shaw
License: Expat

Files:
    vendor/github.com/dustin/gomemcached/*
Copyright:
    2013 Dustin Sallings
License: Expat

Files:
    vendor/github.com/go-ole/go-ole/*
Copyright:
    2013-2017 Yasuhiro Matsumoto <mattn.jp@gmail.com>
License: Expat

Files:
    vendor/github.com/gobwas/*
Copyright:
    2017-2019 Sergey Kamardin <gobwas@gmail.com>
License: Expat

Files:
    vendor/github.com/godror/godror/*
Copyright:
    2016-2020 Tamás Gulácsi
    2016-2020 The Godror Authors
    2016-2020 Oracle and/or its affiliates
License: UPL-1.0 or Apache-2.0

Files:
    vendor/github.com/mediocregopher/radix/*
Copyright: N/A
License: Expat

Files:
    vendor/github.com/memcachier/mc/*
Copyright:
    2015 David Terei
    2011 Blake Mizerany
License: Expat

Files:
    vendor/github.com/natefinch/npipe/*
Copyright:
    2013 npipe authors
    2013 Nate Finch
License: Expat

Files:
    vendor/github.com/omeid/go-yarn/*
Copyright:
    2015 omeid <public@omeid.me>
License: Expat

Files: debian/*
Copyright:
    2009-2012 Christoph Haas <haas@debian.org>
    2012-2022 Dmitry Smirnov <onlyjob@debian.org>
    2006-2009 Fabio Tranchitella <kobold@debian.org>
    2006-2011 Michael Ablassmeier <abi@debian.org>
License: GPL-2+

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 ․
    http://www.apache.org/licenses/LICENSE-2.0
 ․
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ․
 On Debian systems, the complete text of the Apache License,
 Version 2.0 can be found in "/usr/share/common-licenses/Apache-2.0".

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
Comment:
 This license also known as "MIT/X11 (BSD like)" or "MIT" however FSF
 consider "MIT" labelling ambiguous and copyright-format specification
 recommend to label such license as "Expat".

License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 The complete text of the GNU General Public License version 2
 can be found in `/usr/share/common-licenses/GPL-2'.

License: GPL-2
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the Free
 Software Foundation, version 2 of the License.
 ․
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 ․
 On Debian systems, the complete text of the GNU General Public
 License Version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: UPL-1.0
 The Universal Permissive License (UPL), Version 1.0
 ===================================================
 .
 Subject to the condition set forth below, permission is hereby granted to any
 person obtaining a copy of this software, associated documentation and/or data
 (collectively the "Software"), free of charge and under any and all copyright
 rights in the Software, and any and all patent rights owned or freely
 licensable by each licensor hereunder covering either (i) the unmodified
 Software as contributed to or provided by such licensor, or (ii) the Larger
 Works (as defined below), to deal in both
 .
 (a) the Software, and
 .
 (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if
     one is included with the Software (each a "Larger Work" to which the
     Software is contributed by such licensors),
 .
 without restriction, including without limitation the rights to copy, create
 derivative works of, display, perform, and distribute the Software and make,
 use, sell, offer for sale, import, export, have made, and have sold the
 Software and the Larger Work(s), and to sublicense the foregoing rights on
 either these or other terms.
 .
 This license is subject to the following condition:
 .
 The above copyright notice and either this complete permission notice or at a
 minimum a reference to the UPL must be included in all copies or substantial
 portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
